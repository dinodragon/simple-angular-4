import { Injectable }                               from '@angular/core';
import { Http, Response, RequestOptions, Headers }  from '@angular/http';
import { Observable }                               from 'rxjs/Observable';
import { Subscriber }                               from 'rxjs/Subscriber';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Job } from '../models/job';

const HEADERS = new Headers({ 'Content-Type': 'application/json' });
const OPTIONS = new RequestOptions({headers: HEADERS});
let JOBS: Job[] = [];

@Injectable()
export class JobService {
  url: string = 'http://celincapis.azurewebsites.net/api/jobs';

  constructor(private http: Http) {}

  // -----------------------------------------------------------------------------
  getJob(id: string): Observable<Job> {
    if (JOBS.length == 0){
      return this.http.get(`${this.url}/${id}`)
        .map((res: Response) => {
              let body = res.json();
              return body;
            })
        .catch(this.handleError);
    }

    return new Observable<Job>((subscriber: Subscriber<Job>) => {
      let job = JOBS.find(j => j.id == id);
      subscriber.next(job);
    });
  }

  // -----------------------------------------------------------------------------
  getJobs(): Observable<Job[]> {
    // if (JOBS.length != 0){
    //   return new Observable<Job[]>((subscriber: Subscriber<Job[]>) => subscriber.next(JOBS));
    // }

    return this.http.get(this.url)
                    .map((res: Response) => {
                          let body = res.json();
                          JOBS = body;
                          return JOBS;
                        })
                    .catch(this.handleError);
  }

  // -----------------------------------------------------------------------------
  create(job: Job): Observable<any> {
    debugger
    return this.http.post(this.url, job, OPTIONS)
                    .map((res: Response) => {
                      JOBS.push(job);
                      return true;
                    })
                    .catch(this.handleError);
  }

  // -----------------------------------------------------------------------------
  update(job: Job): Observable<any> {
    return this.http.put(`${this.url}/${job.id}`, job, OPTIONS)
                    .map((res: Response) => {
                      let index = JOBS.findIndex(j => j.id == job.id);
                      JOBS.splice(index, 1, job);
                      return true;
                    })
                    .catch(this.handleError);
  }

  // -----------------------------------------------------------------------------
  delete(id: string): Observable<any> {
    return this.http.delete(`${this.url}/${id}`, OPTIONS)
                    .map((res: Response) => {
                      JOBS = JOBS.filter(j => j.id != id);
                      return true;
                    })
                    .catch(this.handleError);
  }

  // -----------------------------------------------------------------------------
  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}