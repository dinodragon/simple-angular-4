export class Job {
  id: string;
  firstName: string;
  lastName: string;
  company: string;
  contactNumber: string;
  email: string;
  title: string;
  description: string;
}
