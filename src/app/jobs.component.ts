import { Component, OnInit }      from '@angular/core';
import { Router }   from '@angular/router';

import { Job }                   from './models/job';
import { JobService }            from './services/job.service';

@Component({
  selector: 'my-heroes',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.css'],
  providers: [JobService]
})

export class JobsComponent implements OnInit {
  jobs: Job[];
  selectedHero: Job;
  errorMessage: String;

  constructor(private heroService: JobService, private router: Router) { }

  ngOnInit(): void {
    this.heroService.getJobs().subscribe(
                       jobs => this.jobs = jobs,
                       error =>  this.errorMessage = <any>error);
  }

  onSelect(id: Number): void {
    this.router.navigate(['/job', id]);
  }
}
