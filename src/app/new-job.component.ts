import { Component, Input }     				from '@angular/core';
import { ActivatedRoute, Params }       from '@angular/router';
import { Location }                     from '@angular/common';
import { Router }   										from '@angular/router';
import 'rxjs/add/operator/switchMap';

import { JobService }                  from './services/job.service';
import { Job }                         from './models/job';

@Component({
  selector: 'new-job',
  templateUrl: './new-job.component.html',
  styleUrls: ['./new-job.component.css'],

})

export class NewJobComponent {
  job: Job = new Job();
  errorMessage: string;
  errors: any = {};
  validator: Object = {};

  constructor(private jobService: JobService, private router: Router, private location: Location) {
    this.validator = {
      firstName: () => {
        if(!this.job.firstName || !this.job.firstName.trim())
          this.errors['firstName'] = 'first name is required';
      },
      lastName: () => {
        if(!this.job.lastName || !this.job.lastName.trim())
          this.errors['lastName'] = 'last name is required';
      },
      company: () => {
        if(!this.job.company || !this.job.company.trim())
          this.errors['company'] = 'company is required';
      },
      contactNumber: () => {
        if(!this.job.contactNumber || !this.job.contactNumber.trim())
          this.errors['contactNumber'] = 'contact number is required';
      },
      email: () => {
        if(!this.job.email || !this.job.email.trim())
          this.errors['email'] = 'email is required';

        let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!pattern.test(this.job.email))
          this.errors['email'] = 'please enter a valid email address';
      },
      title: () => {
        if(!this.job.title || !this.job.title.trim())
          this.errors['title'] = 'title is required';
      }
    }
  }

  onSave(): void{
    Object.keys(this.validator).forEach(k => this.validator[k]());
    if(Object.keys(this.errors).length > 0) return;

  	this.jobService.create(this.job).subscribe(
      resp => this.router.navigate(['/jobs']),
      error => this.errorMessage = <any>error);
  }

  onBack(): void{
  	this.location.back();
  }

  onChange(name, value): void{
    delete this.errors[name];
    this.validator[name]();
  }

  onFormInvalid(): boolean {
    return Object.keys(this.errors).length > 0;
  }
}
