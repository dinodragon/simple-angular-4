import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent }   from './dashboard.component';
import { JobsComponent }      	from './jobs.component';
import { JobDetailComponent }  	from './job-detail.component';
import { NewJobComponent }  	from './new-job.component';
import { EditJobComponent }  	from './edit-job.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard',  	component: DashboardComponent },
  { path: 'job/new',    	component: NewJobComponent },
  { path: 'job/:id', 	component: JobDetailComponent },
  { path: 'job/:id/edit', 	component: EditJobComponent },
  { path: 'jobs',     		component: JobsComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
