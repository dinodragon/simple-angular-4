import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  	<div class="ani-fadein">
	  	<h2>Welcome to jobspool.</h2>
	  	<h2>Without further a do</h2>
	  	<a routerLink="/jobs"><h2>Let's get started!</h2></a>
	</div>
  `,
  styles:['div{ text-align:center; }']
})

export class DashboardComponent {}
