import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule }   from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MdButtonModule, 
  MdCheckboxModule, 
  MdCardModule, 
  MdInputModule,
  MdGridListModule } from '@angular/material';
import 'hammerjs';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { JobDetailComponent } from './job-detail.component';
import { JobsComponent }     from './jobs.component';
import { DashboardComponent }     from './dashboard.component';
import { NewJobComponent }     from './new-job.component';
import { EditJobComponent }    from './edit-job.component';
import { JobService }         from './services/job.service';

@NgModule({
  declarations: [
    AppComponent,
    JobDetailComponent,
    JobsComponent,
    DashboardComponent,
    NewJobComponent,
    EditJobComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MdButtonModule,
    MdCheckboxModule,
    MdCardModule,
    MdInputModule,
    MdGridListModule
  ],
  providers: [
    JobService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
