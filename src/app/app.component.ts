import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <div class='navbar'>
      <div class="container">
        <div class="col-xs-4">
          <button class='logo' md-button routerLink=".">JobsPool</button>
        </div>
        <div class='nav-action col-xs-8'>
          <button class="add" md-button routerLink="/job/new">
            <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            New Vacancy
           </button>
        </div>
      </div>
    </div>
    <div class='container'>
      <router-outlet></router-outlet>
    </div>
  `,
  styles: [`
    .logo {
      font-size: 30px;
      font-weight: 300;
      height: 50px;
      margin-top: -1px;
    }

    button.add { padding: 6px 10px; }
  `]
})

export class AppComponent {
  title = 'Tour of Heroes';
}
