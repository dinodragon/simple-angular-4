import { Component, Input, OnInit }     from '@angular/core';
import { ActivatedRoute, Params }       from '@angular/router';
import { Location }                     from '@angular/common';
import { Router }                       from '@angular/router';
import 'rxjs/add/operator/switchMap';

import { JobService }                  from './services/job.service';
import { Job }                         from './models/job';

@Component({
  selector: 'job-detail',
  templateUrl: './job-detail.component.html',
  styleUrls: ['./job-detail.component.css']
})

export class JobDetailComponent implements OnInit{
  constructor(private jobService: JobService, private route: ActivatedRoute, private location: Location, private router: Router) {}
  
  job: Job;
  errorMessage: string;

  ngOnInit(): void {
    this.route.params
      .switchMap((params: Params) => this.jobService.getJob(params['id']))
      .subscribe(job => {
      	this.job = job
      });
  }

  onDelete(): void {
    this.jobService.delete(this.job.id).subscribe(
      resp => this.router.navigate(['/jobs']),
      error => this.errorMessage = <any>error);
  }

  onBack(): void {
    this.location.back();
  }

}
