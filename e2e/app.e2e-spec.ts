import { HelloAg2Page } from './app.po';

describe('hello-ag2 App', () => {
  let page: HelloAg2Page;

  beforeEach(() => {
    page = new HelloAg2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
